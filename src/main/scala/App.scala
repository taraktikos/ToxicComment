import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.NaiveBayes
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.feature._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, concat, split}
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.{SaveMode, SparkSession}

object App {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.master("local[*]").appName("Ranker").getOrCreate()


    println("test")
    var trainDF = spark
      .read
      .format("com.databricks.spark.csv")
      .option("multiLine", "true")
      .option("escape", "\"")
      .option("header", "true")
      .load("trainToxic.csv")
      .cache()
//      .limit(100)

    trainDF.show()
    val testDF = spark.read.option("header", "true")
      .option("parserLib", "univocity")
      .option("multiLine", "true")
      .option("inferSchema", "true")
      .option("escape", "\"").format("com.databricks.spark.csv")
      .load("testToxic.csv")
//      .limit(100)

    var resDF = testDF.select("id")
    val labels = Array("toxic", "severe_toxic", "obscene", "threat", "insult", "identity_hate")
    for (label <- labels) {
      val resDF1 = cr(label, trainDF.select(col("id"), col("comment_text"), col(label).cast("Double").as("label")), testDF)
      resDF = resDF.join(resDF1, "id")
    }

    resDF
      .repartition(1)
      .write
      .mode(SaveMode.Overwrite)
      .format("com.databricks.spark.csv")
      .option("header", value = true)
      .save("sparkResultScala.csv")

    spark.stop()
  }

  def cr(label: String, trainDF: DataFrame, testDF: DataFrame): DataFrame = {


    val tokenizer = new Tokenizer().setInputCol("comment_text").setOutputCol("tokens")
    val stopWordsRemover = new StopWordsRemover().setStopWords(StopWordsRemover.loadDefaultStopWords("english")).setInputCol("tokens").setOutputCol("words")
    val hashingTF = new HashingTF().setInputCol("words").setOutputCol("rawFeatures")
    val idf = new IDF().setInputCol("rawFeatures").setOutputCol("features")
    val pipeline = new Pipeline().setStages(Array(tokenizer, stopWordsRemover, hashingTF, idf))

    System.out.println("Created pipeline")

    val trainDF2 = pipeline.fit(trainDF).transform(trainDF)
    val splits = trainDF2.randomSplit(Array[Double](0.7, 0.3))
    val trainingData = splits(0)
    val validationData = splits(1)

    System.out.println("Prepared training and validation data")

    /// NaiveBayes start
    val naiveBayes = new NaiveBayes()
    val naiveBayesModel = naiveBayes.fit(trainingData)
    val predictions = naiveBayesModel.transform(validationData)
    val evaluator = new MulticlassClassificationEvaluator().setLabelCol("label").setPredictionCol("prediction").setMetricName("accuracy")
    val accuracy = evaluator.evaluate(predictions)
    System.out.println("NaiveBayes accuracy = " + accuracy)
    /// NaiveBayes end


    val transformedTestData = pipeline.fit(testDF).transform(testDF)
    val result = naiveBayesModel.transform(transformedTestData)

    result.show()

    return result.select(col("id"), col("prediction").alias(label))

  }
}
